Life In Space
=============

This game/interactive fiction is my entry into the 2015 LibGDXJam Game Jam, hosted at [itch.io](http://itch.io/jam/libgdxjam).  It is designed for Android, to be a short but interesting little story.

You can follow my development at http://itch.io/jam/libgdxjam/topic/12031/gauauus-log.

You can download the apk at http://gauauu.itch.io/search-for-life-in-space