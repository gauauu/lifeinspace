package net.tolberts.android.lifeinspace;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class PhoneCallStateTracker implements StateTracker {
    private final StarsScreen screen;
    private Image phoneCallOverlay;
    private BitmapFont timeFont;
    private float timer;
    private MyTimerActor timerActor;
    private boolean started;

    public PhoneCallStateTracker(StarsScreen starsScreen) {
        this.screen = starsScreen;
        this.screen.game.progress.setState(Progress.STATE_CALL);
        start();
    }

    @Override
    public void start() {
        started = false;
        phoneCallOverlay = new Image(screen.game.assetMgr.get(StarsScreen.IMG_CALL_SCREEN, Texture.class));
        phoneCallOverlay.setWidth(LifeInSpaceGame.WIDTH);
        phoneCallOverlay.setHeight(LifeInSpaceGame.HEIGHT);
        phoneCallOverlay.setPosition(0, 0);
        phoneCallOverlay.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                checkForHangup(x,y);
                Gdx.app.error("test", "" + x + "," + y);
            }
        });
        screen.stage.addActor(phoneCallOverlay);

        timerActor = new MyTimerActor();
        timerActor.setWidth(LifeInSpaceGame.WIDTH);
        timerActor.setHeight(LifeInSpaceGame.HEIGHT);
        timerActor.setPosition(0, 0);
        timerActor.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                checkForHangup(x,y);
                Gdx.app.error("test", "" + x + "," + y);
            }
        });
        screen.stage.addActor(timerActor);

        timer = 0;

    }

    void checkForHangup(float x, float y) {
        if (x > 151.0f && x < 222.0f && y > 53.0f && y < 131.0f) {
            hangup();
        }
    }

    void hangup() {
        screen.game.platformAdapter.stopPhoneCallAudio();
        screen.game.platformAdapter.playHangupSound();
        goToNextStage();
    }

    void goToNextStage() {
        timerActor.remove();
        phoneCallOverlay.remove();
        screen.game.progress.setState(Progress.STATE_WIPING);
        screen.stateTracker = new WipingStateTracker(screen);
    }

    private BitmapFont getTimeFont() {

        if (timeFont == null) {

            FileHandle fontFile = Gdx.files.internal("fonts/Roboto-Light.ttf");
            FreeTypeFontGenerator generator = new FreeTypeFontGenerator(fontFile);
            FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
            parameter.size = 20;
            parameter.characters = "0123456789:";
            timeFont = generator.generateFont(parameter);
            generator.dispose();
        }
        return timeFont;
    }

    @Override
    public void update(float delta) {
        timer += delta;
        if (timer > 0.5f && !started) {
            started = true;
            screen.game.platformAdapter.startPhoneCallAudio();
        }
        if (timer > 45.0f) {
            hangup();
        }
    }

    private class MyTimerActor extends Actor {

        @Override
        public void draw(Batch batch, float parentAlpha) {
            int seconds = (int)timer;
            String secondsStr = "" + seconds;
            if (secondsStr.length() == 1) {
                secondsStr = "0" + secondsStr;
            }
            getTimeFont().setColor(Color.WHITE);
            getTimeFont().draw(batch, "00:" + secondsStr, LifeInSpaceGame.WIDTH - 90, LifeInSpaceGame.HEIGHT - 120);

        }
    }

}
