package net.tolberts.android.lifeinspace;

public class Statics {
    public static final int STATIC_NONE = -2;
    public static int STATIC_ALIENS = 8;
    public static String[] staticFile = new String[]{
            "ScatterNoise1",
            "ScatterNoise2",
            "ScatterNoise3",
            "static",
            "static2",
            "continuousStatic1",
            "continuousStatic2",
            "continuousStatic3",
            "alienNoise6",
            "alienNoise3",
            "alienNoise4",
            "alienNoise5",
            "alienNoise1",
            "alienNoise2",
            "alienNoise6",
            "alienNoise6"
    };

    public static String getStaticAudioFile(int staticId) {
        return "audio/" + staticFile[staticId] + ".mp3";
    }

    public static String getStaticImageFile(int staticId) {
        return "img/static/" + staticFile[staticId] + ".png";
    }

    public static int getStaticFor(int staticType, boolean button1, boolean button2, boolean button3) {
        int original = staticType;
        staticType += (button1 ? 1 : 0);
        staticType += (button2 ? 2 : 0);
        staticType += (button3 ? 4 : 0);
        if (original < 8 && staticType >= 8) {
            staticType -= 8;
        }
        return staticType;
    }
}
