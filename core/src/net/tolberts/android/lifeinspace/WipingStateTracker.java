package net.tolberts.android.lifeinspace;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

public class WipingStateTracker implements StateTracker {
    private final StarsScreen screen;
    private float timer;
    private Image background;
    private Label label;

    private String textSoFar = "";
    private String textToWrite = "Incoming \n"+
                                " Remote Connection\n" +
                                " from carnivore.cia.gov\n\n" +
                                " .....reading phone logs\n"+
                                " .....\n"+
                                " .....uploading logs\n"+
                                " .....\n" +
                                " .....disabling app\n"+
                                " .....\n" +
                                " COMPLETED\n\n";
    int charsSoFar;



    public WipingStateTracker(StarsScreen screen) {
        this.screen = screen;
        start();
    }

    @Override
    public void start() {
        timer = 0.0f;
        background = new Image(screen.game.assetMgr.get(StarsScreen.IMG_HACK_BACKDROP, Texture.class));
        background.setWidth(150);
        background.setHeight(150);
        background.setPosition(200, 100);
        background.setVisible(false);
        screen.stage.addActor(background);
        label = new Label("", screen.getSkin(), "default-font", Color.WHITE);
        label.setVisible(false);
        label.setPosition(background.getX() + 10, background.getY() + 10);
        screen.stage.addActor(label);
    }

    @Override
    public void update(float delta) {
        timer += delta;
        if (timer > 3.0f) {
            background.setVisible(true);
            background.setPosition(LifeInSpaceGame.WIDTH / 5, LifeInSpaceGame.HEIGHT / 3);
            background.setSize(LifeInSpaceGame.WIDTH / 5 * 3, LifeInSpaceGame.HEIGHT / 2);
            label.setVisible(true);
            label.setPosition(background.getX() + 10, background.getY() + background.getHeight() / 2);
            label.setWrap(true);
            label.setWidth(background.getWidth() - 20);

            float timePerCharacter = 0.1f;

            if (timer > 3.0f + (textSoFar.length() * timePerCharacter)) {
                if (textSoFar.length() < textToWrite.length()) {
                    textSoFar = textSoFar + textToWrite.charAt(textSoFar.length());
                } else {
                    if (timer > 6.0f + (textSoFar.length() * timePerCharacter)) {
                        screen.game.progress.setGameComplete();
                        //purposely crash....
                        Object nul = null;
                        nul.toString();
                    }
                }
                label.setText(textSoFar);
            }
        }

    }
}
