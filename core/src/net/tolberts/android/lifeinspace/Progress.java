package net.tolberts.android.lifeinspace;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Preferences;

public class Progress {

    public final static int STATE_START = 0;
    public final static int STATE_INSTRUCTIONS = 1;
    public final static int STATE_SEARCH = 2;
    public final static int STATE_ALIENS = 3;
    public final static int STATE_FACEDOWN = 4;
    public final static int STATE_RINGING = 5;
    public final static int STATE_CALL = 6;
    public final static int STATE_CALL_OVER = 7;
    public final static int STATE_WIPING = 8;
    public final static int STATE_CRASH = 9;

    float stateTimer;
    int state;
    private boolean didJustSwitch;

    int timesClicked = 0;

    public Progress() {
        state = STATE_START;
        stateTimer = 0;
    }

    public void loadAndCrashIfCompleted() {
        if (Gdx.input.isKeyPressed(Input.Keys.Y)){
            resetGameComplete();
        }
        boolean available = Gdx.input.isPeripheralAvailable(Input.Peripheral.Accelerometer);
        if (available) {
            float accelZ = Gdx.input.getAccelerometerZ();
            if (accelZ < -7.0f) {
                resetGameComplete();
            }
        }
        if (isGameCompleted()) {
            Object nul = null;
            nul.toString();
        }
    }


    public boolean isFinished() {
        return false;
    }

    public void timeTick(float delta) {
        stateTimer += delta;


    }

    public boolean isNextState() {
        if (didJustSwitch) {
            didJustSwitch = false;
            return true;
        }
        switch (state) {
            case STATE_START:
                if (stateTimer > 1) {
                    state = STATE_INSTRUCTIONS;
                    return true;
                }
                break;
        }

        return false;
    }

    public void setStateCompleted() {
        state++;
        stateTimer = 0;
        didJustSwitch = true;
    }

    public void setState(int state) {
        stateTimer = 0;
        this.state = state;
        didJustSwitch = true;
    }

    public void setGameComplete() {
        Preferences prefs = Gdx.app.getPreferences("sfls");
        prefs.putBoolean("disabled", true);
        prefs.flush();
    }

    public boolean isGameCompleted() {
        Preferences prefs = Gdx.app.getPreferences("sfls");
        return prefs.getBoolean("disabled", false);
    }

    private void resetGameComplete() {
        Preferences prefs = Gdx.app.getPreferences("sfls");
        prefs.putBoolean("disabled", false);
        prefs.flush();
    }


}
