package net.tolberts.android.lifeinspace;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class LifeInSpaceGame extends ApplicationAdapter {

    public static final int WIDTH = 360;
    public static final int HEIGHT =640;
    final PlatformAdapter platformAdapter;
    AssetManager assetMgr = new AssetManager();
    Progress progress = new Progress();
	SpriteBatch batch;
	Texture img;
    Screen screen;

    public LifeInSpaceGame(PlatformAdapter platformAdapter) {
        this.platformAdapter = platformAdapter;
    }

    @Override
	public void create () {
        progress.loadAndCrashIfCompleted();
        if (progress.isFinished()) {
             setScreen(new BlankScreen());

        } else {
            StarsScreen mainScreen = new StarsScreen(this);
            mainScreen.setProgress(progress);
            setScreen(mainScreen);
        }
		batch = new SpriteBatch();
	}


    @Override
    public void render () {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
//        batch.draw(img, 0, 0);
        batch.end();
        if (screen != null) {
            screen.render(Gdx.graphics.getDeltaTime());
        }
    }

    public void setScreen(Screen screen) {
        if (this.screen != null) this.screen.hide();
        this.screen = screen;
        if (this.screen != null) {
            this.screen.show();
            this.screen.resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        }
        this.screen = screen;
    }

    @Override
    public void resume() {
        progress.loadAndCrashIfCompleted();
        super.resume();
    }
}
