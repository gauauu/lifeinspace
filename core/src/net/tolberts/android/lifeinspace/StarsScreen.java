package net.tolberts.android.lifeinspace;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import java.util.Random;

public class StarsScreen extends ScreenAdapter {

    public static final String IMG_STARS01 = "img/spr_stars01.png";
    public static final String IMG_STARS02 = "img/spr_stars02.png";
    public static final String IMG_STARS_3 = "img/stars3.png";
    public static final String IMG_FRAME = "img/frame.png";
    public static final String ASSET_SKIN = "skin/uiskin.json";
    private static final String IMG_RETICULE = "img/reticule.png";
    private static final String IMG_INCOMING_CALL = "img/incomingCall.png";
    static final String IMG_CALL_SCREEN = "img/currentCall.png";
    public static String IMG_HACK_BACKDROP = "img/black.png";
    private final Texture starsToDraw;
    private final Texture frame;
    private final SpriteBatch batch;
    final Stage stage;
    private final Image reticule;
    private final Random random;
    private final OrthographicCamera camera;
    private Progress progress;
    LifeInSpaceGame game;
    float scroll = 0.0f;

    float reticuleX = -1000.0f;
    float reticuleY = -10.0f;
    private int staticType = Statics.STATIC_NONE;
    private float staticShownTimer = 0;
    private Music music;
    private TextButton filter1;
    private TextButton filter2;
    private TextButton filter3;
    private float aliensX = -1000;
    private float aliensY = -1000;
    private final float TIME_BEFORE_FOUND = 3;
    StateTracker stateTracker = null;

    public StarsScreen(LifeInSpaceGame game) {
        super();
        this.game = game;
        random = new Random();
//        game.assetMgr.load(IMG_STARS01, Texture.class);
//        game.assetMgr.load(IMG_STARS02, Texture.class);
        game.assetMgr.load(IMG_STARS_3, Texture.class);
        game.assetMgr.load(IMG_FRAME, Texture.class);
        game.assetMgr.load(IMG_RETICULE, Texture.class);
        game.assetMgr.load(IMG_INCOMING_CALL, Texture.class);
        game.assetMgr.load(IMG_CALL_SCREEN, Texture.class);
        game.assetMgr.load(IMG_HACK_BACKDROP, Texture.class);
        game.assetMgr.load(ASSET_SKIN, Skin.class);
        for (int i = 0; i < Statics.staticFile.length; i++) {
            String audioFile = Statics.getStaticAudioFile(i);
//            game.assetMgr.load(audioFile, Music.class);

            String imageFile = Statics.getStaticImageFile(i);
            game.assetMgr.load(imageFile, Texture.class);
        }
//        game.assetMgr.load(AlienConversationStateTracker.BRANCH_MAIN, Music.class);
//        game.assetMgr.load(AlienConversationStateTracker.BRANCH_NO_1, Music.class);
//        game.assetMgr.load(AlienConversationStateTracker.BRANCH_NO_2, Music.class);
//        game.assetMgr.load(AlienConversationStateTracker.BRANCH_YES_1, Music.class);
//        game.assetMgr.load(AlienConversationStateTracker.BRANCH_YES_2, Music.class);
        game.assetMgr.finishLoading();
        starsToDraw = game.assetMgr.get(IMG_STARS_3);
        frame = game.assetMgr.get(IMG_FRAME);
        stage = new Stage(new StretchViewport(LifeInSpaceGame.WIDTH, LifeInSpaceGame.HEIGHT));
        batch = new SpriteBatch();

        Window window = buildControlWindow();
        Image starfield = new Image(starsToDraw);
        starfield.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
//                targetPosition(x, y);
                return super.touchDown(event, x, y, pointer, button);
            }

            @Override
            public void clicked(InputEvent event, float x, float y) {
                targetPosition(x, y);
                super.clicked(event, x, y);
            }
        });
        starfield.setPosition(0, 0);
        starfield.setSize(LifeInSpaceGame.WIDTH, LifeInSpaceGame.HEIGHT);


        reticule = new Image(game.assetMgr.get(IMG_RETICULE, Texture.class));
        reticule.setTouchable(Touchable.disabled);

        stage.addActor(starfield);
        stage.addActor(reticule);
        stage.addActor(window);

        camera = new OrthographicCamera(LifeInSpaceGame.WIDTH, LifeInSpaceGame.HEIGHT);
//        camera.lookAt(LifeInSpaceGame.WIDTH / 2, LifeInSpaceGame.HEIGHT / 2, 0);
        camera.update();

        Gdx.input.setInputProcessor(stage);
    }

    private void targetPosition(float x, float y) {
        if (game.progress.state == Progress.STATE_SEARCH) {
            reticuleX = x - reticule.getWidth() / 2;
            reticuleY = y - reticule.getHeight() / 2;

            staticType = random.nextInt(8);
            game.progress.timesClicked++;
            if (game.progress.stateTimer > TIME_BEFORE_FOUND && game.progress.timesClicked > 5 && aliensX < -100) {
                assignAliens(x, y);
            }
            if (aliensX > -100 && x > aliensX - 15 && x < aliensX + 15 && y > aliensY - 15 && y < aliensY + 15) {
                staticType = Statics.STATIC_ALIENS;
            }
            playStaticNoise();
            staticShownTimer = 0;
        }
    }

    private void assignAliens(float x, float y) {
        aliensX = x;
        aliensY = y;
        staticType = Statics.STATIC_ALIENS;
    }

    private void playStaticNoise() {
        if (music != null) {
            music.stop();
            music.dispose();
        }
        if (staticType == Statics.STATIC_NONE) {
            return;
        }
//        music = game.assetMgr.get(Statics.getStaticAudioFile(getFilteredStaticId()), Music.class);
        music = Gdx.audio.newMusic(Gdx.files.internal(Statics.getStaticAudioFile(getFilteredStaticId())));
        music.setLooping(true);
        music.play();
    }

    private int getFilteredStaticId() {
        return Statics.getStaticFor(staticType, filter1.isChecked(), filter2.isChecked(), filter3.isChecked());
    }

    private Window buildControlWindow() {
        float paddingLeft = 120;

        Window window = new Window("Signal", getSkin());
        window.setPosition(0, 0);
        window.setSize(LifeInSpaceGame.WIDTH, LifeInSpaceGame.HEIGHT / 3);
        window.setMovable(false);
        window.setResizable(false);
        MyLabel label = new MyLabel("Radio Filters", getSkin());
        window.add(label).padLeft(paddingLeft).colspan(3).expandX();
        window.row();

        filter1 = new TextButton("     Filter 1     ", getSkin(), "bluetoggle");
        filter1.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                playStaticNoise();
            }
        });
        window.add(filter1).padLeft(paddingLeft).expandX().padTop(10);
        window.row();

        filter2 = new TextButton("     Filter 2     ", getSkin(), "bluetoggle");
        filter2.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                playStaticNoise();
            }
        });
        window.add(filter2).padLeft(paddingLeft).expandX().padTop(10);
        window.row();

        filter3 = new TextButton("     Filter 3     ", getSkin(), "bluetoggle");
        filter3.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                playStaticNoise();
            }
        });
        window.add(filter3).padLeft(paddingLeft).expandX().padTop(10);
        window.row();

        return window;
    }

    Skin getSkin() {
        return game.assetMgr.get(ASSET_SKIN, Skin.class);
    }

    @Override
    public void show() {
        super.show();
    }

    public void setProgress(Progress progress) {
        this.progress = progress;
    }

    @Override
    public void render(float delta) {
        game.assetMgr.update();
        super.render(delta);
        updateScreen(delta);
        renderScreen(delta);
    }

    private void renderScreen(float delta) {
        stage.draw();
        if (staticType != Statics.STATIC_NONE) {
            if (game.progress.state == Progress.STATE_SEARCH || game.progress.state == Progress.STATE_ALIENS) {
                drawStaticWaveform();
            }
        }
    }

    private void drawStaticWaveform() {

        camera.position.set(LifeInSpaceGame.WIDTH / 2, LifeInSpaceGame.HEIGHT / 2, 0);
        camera.update();
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        Texture staticTexture = getStaticTexture();
        int sourceX = (int) (staticShownTimer * 20);
        sourceX = sourceX % staticTexture.getWidth();

        batch.draw(staticTexture, 10, 40, 150, 100, sourceX, 0, 150, staticTexture.getHeight(), false, false);
        batch.end();
    }

    private void updateScreen(float delta) {

        scroll -= delta * 0.8f;
        reticule.setVisible(reticuleX > -1000);
        reticule.setPosition(reticuleX, reticuleY);
        if (staticType != Statics.STATIC_NONE) {
            staticShownTimer += delta;
        }

        stage.act(delta);
        game.progress.timeTick(delta);

        if (this.stateTracker != null) {
            this.stateTracker.update(delta);
        }

        if (game.progress.state == Progress.STATE_SEARCH
                && staticType == Statics.STATIC_ALIENS
                && !filter1.isChecked()
                && filter2.isChecked()
                && filter3.isChecked()
                && staticShownTimer >= 5) {
//            game.progress.setStateCompleted();
//            game.progress.state = Progress.STATE_RINGING;
            if (music != null) {
                music.stop();
                music.dispose();
            }
            game.progress.setStateCompleted();
            game.progress.setState(Progress.STATE_ALIENS);
            this.stateTracker = new AlienConversationStateTracker(this);
        }

        if (game.progress.isNextState()) {
            if (game.progress.state == Progress.STATE_INSTRUCTIONS) {
                showInstructions();
            }
            if (game.progress.state == Progress.STATE_RINGING) {
                doIncomingCall();
            }
        }

        if (game.progress.state == Progress.STATE_RINGING) {
            game.platformAdapter.keepPlayingRingtone();
        }
    }

    private void doIncomingCall() {
        if (music != null) {
            music.stop();
            music.dispose();
        }
        game.platformAdapter.playRingtone();
//        music = game.platformAdapter.getRingtone();
//        music.setLooping(true);
//        music.play();
        Texture texture = game.assetMgr.get(IMG_INCOMING_CALL, Texture.class);
        TextureRegionDrawable drawable = new TextureRegionDrawable(new TextureRegion(texture));
        final Image image = new Image(drawable);
        image.setWidth(LifeInSpaceGame.WIDTH);
        image.setHeight(120);
        image.setPosition(0, LifeInSpaceGame.HEIGHT - 120);
        image.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (x < LifeInSpaceGame.WIDTH / 2) {
                    game.platformAdapter.stopRingtone();
                    game.progress.setState(Progress.STATE_WIPING);
                    stateTracker = new WipingStateTracker(StarsScreen.this);
                } else {
                    game.platformAdapter.stopRingtone();
                    startPhonecall();
                }
                image.setVisible(false);
                image.remove();
            }
        });
        stage.addActor(image);

    }

    private void startPhonecall() {
        stateTracker = new PhoneCallStateTracker(this);
;
    }

    private void showInstructions() {
        final MyLabel link = new MyLabel("http://www.searchforlife.space", getSkin(), "default-font", Color.BLUE);
        link.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.net.openURI("http://www.searchforlife.space");
            }
        });
        link.setPosition(108, 182);

        final Dialog dialog = new Dialog("Welcome", getSkin());
        MyLabel label = new MyLabel(getInstructionText(), getSkin());
//        label.setWrap(true);
//        label.setAlignment(Align.left, Align.left);
        dialog.getContentTable().add(label).expand();
//        label.setWidth(dialog.getWidth());
        Button okButton = new TextButton("Ok", getSkin(), "blue");
        okButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.progress.setStateCompleted();
                dialog.remove();
                link.remove();

                super.clicked(event, x, y);
            }
        });
        dialog.getContentTable().row();
        dialog.getContentTable().add(okButton);
        dialog.setWidth(LifeInSpaceGame.WIDTH - 20);
        dialog.setHeight(LifeInSpaceGame.HEIGHT - 50);
        dialog.setPosition(10, 25);
        stage.addActor(dialog);

        stage.addActor(link);

    }

    public String getInstructionText() {
        return "Welcome to the Search For Life\nIn Space (SFLS) companion app!\n" +
                "We appreciate your interest in our\n" +
                "search for life beyond earth,\n" +
                "and want to thank you for helping\n" +
                "in our mission.\n\n" +

                "To use this app, make sure\n" +
                "your volume is up. Tap a star\n" +
                "on the starmap to listen to the\n" +
                "radio signals from that star.\n\n" +

                "You will want to use the various\n"+
                "signal filters to help you determine\n"+
                "if there is a signal embedded in the noise.\n\n"+

                "If you find anything of note, or have any\n"+
                "questions, please report it to our\n"+
                "website at \n\n" + //searchforlifeinspace.org\n\n" +

                "Thank you, and happy searching!\nGood luck!";


    }

    public Texture getStaticTexture() {
        return game.assetMgr.get(Statics.getStaticImageFile(getFilteredStaticId()), Texture.class);
    }
}
