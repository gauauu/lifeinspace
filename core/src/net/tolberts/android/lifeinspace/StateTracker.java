package net.tolberts.android.lifeinspace;

public interface StateTracker {
    public void start();
    public void update(float delta);
}
