package net.tolberts.android.lifeinspace;

public interface PlatformAdapter {

    public static final int ORIENT_UP = 1;
    public static final int ORIENT_DOWN = 2;
    int OUTPUT_NORMAL= 1;
    int OUTPUT_PHONE_SPEAKER = 2;
    int ORIENT_UNAVAILABLE = -100;

    void playHangupSound();

    void startPhoneCallAudio();


    public void playRingtone();
    public void stopRingtone();
    public void keepPlayingRingtone();

    public void setAudioOutput(int output);

    void stopPhoneCallAudio();

    void vibrateIfShouldOnRing();
}
