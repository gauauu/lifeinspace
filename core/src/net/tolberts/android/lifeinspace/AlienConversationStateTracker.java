package net.tolberts.android.lifeinspace;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;

public class AlienConversationStateTracker implements StateTracker {

    public static final String BRANCH_MAIN= "audio/alien/main.mp3";
    public static final String BRANCH_NO_1= "audio/alien/no_1.mp3";
    public static final String BRANCH_YES_1= "audio/alien/yes_1.mp3";
    public static final String BRANCH_YES_2= "audio/alien/yes_2.mp3";
    public static final String BRANCH_NO_2= "audio/alien/no_2.mp3";

    public static final float TIME_WAIT_RESPONSE = 4.0f;

    private final StarsScreen screen;

    Music music = null;
    float timer = 0;
    private String currentBranch;
    private boolean isFinished;

    public AlienConversationStateTracker(StarsScreen starsScreen) {
        this.screen = starsScreen;
        start();
    }

    @Override
    public void start() {
        playBranch(BRANCH_MAIN);
    }

    @Override
    public void update(float delta) {
        timer += delta;
        if (isFinished && timer > TIME_WAIT_RESPONSE) {
            //the first branch
            if (BRANCH_MAIN.equals(currentBranch)) {
                if (isFaceDown()) {
                    playBranch(BRANCH_YES_1);
                } else {
                    playBranch(BRANCH_NO_1);
                }
                return;
            }

            //second branches
            if (BRANCH_YES_1.equals(currentBranch)) {
                if (isFaceDown()) {
                    playBranch(BRANCH_YES_2);
                } else {
                    playBranch(BRANCH_NO_2);
                }
            }

            return;

        }

        //if all the way done
        if (isFinished && (BRANCH_YES_2.equals(currentBranch) || BRANCH_NO_2.equals(currentBranch) || BRANCH_NO_1.equals(currentBranch))) {
            //go to call
            screen.game.progress.setState(Progress.STATE_RINGING);
            screen.stateTracker = null;
            return;
        }
    }

    public boolean isFaceDown() {
        boolean available = Gdx.input.isPeripheralAvailable(Input.Peripheral.Accelerometer);
        if (available) {
            float accelZ = Gdx.input.getAccelerometerZ();
            return (accelZ < -7.0f);
        }
        return (Gdx.input.isKeyPressed(Input.Keys.Y));
    }

    void playBranch(String branch) {
        if (music != null) {
            music.stop();
        }
//        music = screen.game.assetMgr.get(branch, Music.class);
        music = Gdx.audio.newMusic(Gdx.files.internal(branch));
        music.setLooping(false);
        music.play();
        timer = 0.0f;
        currentBranch = branch;
        isFinished = false;
        music.setOnCompletionListener(new Music.OnCompletionListener() {
            @Override
            public void onCompletion(Music music) {
                isFinished = true;
                timer = 0.0f;
            }
        });
    }

}
