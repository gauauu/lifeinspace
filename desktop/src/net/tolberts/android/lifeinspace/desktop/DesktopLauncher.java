package net.tolberts.android.lifeinspace.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import net.tolberts.android.lifeinspace.LifeInSpaceGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = LifeInSpaceGame.WIDTH;
        config.height = LifeInSpaceGame.HEIGHT;
        config.title = "SFLS Companion App";
        config.useGL30 = true;
		new LwjglApplication(new LifeInSpaceGame(new DesktopAdapter()), config);
	}
}
