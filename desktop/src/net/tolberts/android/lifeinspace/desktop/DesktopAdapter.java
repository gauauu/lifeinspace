package net.tolberts.android.lifeinspace.desktop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import net.tolberts.android.lifeinspace.PlatformAdapter;

public class DesktopAdapter implements PlatformAdapter{

    private Music ringtone;
    private Music phoneCallMusic;

    private Music getRingtone() {
        if (ringtone == null) {
            ringtone = Gdx.audio.newMusic(Gdx.files.internal("audio/phoneRing.mp3"));
        }
        return ringtone;
    }

    @Override
    public void playHangupSound() {

    }

    @Override
    public void startPhoneCallAudio() {
        phoneCallMusic = Gdx.audio.newMusic(Gdx.files.internal("audio/governmentCall.mp3"));
        if (phoneCallMusic != null) {
            phoneCallMusic.play();
        }
    }



    @Override
    public void playRingtone() {
        Music ringtone = getRingtone();
        ringtone.setLooping(true);
        ringtone.play();
    }

    @Override
    public void stopRingtone() {
        getRingtone().stop();
    }

    @Override
    public void keepPlayingRingtone() {
        //do nothing, it keeps playing automatically
    }

    @Override
    public void setAudioOutput(int output) {
        //do nothing, we only support standard output
    }

    @Override
    public void stopPhoneCallAudio() {
        if (phoneCallMusic != null) {
            phoneCallMusic.stop();
        }
    }

    @Override
    public void vibrateIfShouldOnRing() {

    }
}
