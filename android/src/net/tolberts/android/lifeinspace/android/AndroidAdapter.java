package net.tolberts.android.lifeinspace.android;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.*;
import android.net.Uri;
import android.provider.Settings;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.backends.android.AndroidFileHandle;
import net.tolberts.android.lifeinspace.PlatformAdapter;

public class AndroidAdapter implements PlatformAdapter {

    private final Activity activity;
    private Ringtone defaultRingtone;
    private MediaPlayer mediaPlayer;

    public AndroidAdapter(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void playRingtone() {

        Uri defaultRingtoneUri = RingtoneManager.getActualDefaultRingtoneUri(activity.getApplicationContext(), RingtoneManager.TYPE_RINGTONE);
        defaultRingtone = RingtoneManager.getRingtone(activity, defaultRingtoneUri);
        defaultRingtone.play();



//        try {
//            Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//            String defaultPath = uri.getPath();
//            String defaultPath = Settings.System.DEFAULT_NOTIFICATION_URI.getPath();
//            return Gdx.audio.newMusic(Gdx.files.absolute(defaultPath));
//        } catch (Exception e) {
//            return Gdx.audio.newMusic(Gdx.files.internal("audio/phoneRing.mp3"));
//        }
    }

    public void stopRingtone() {
        if (defaultRingtone != null) {
            defaultRingtone.stop();
        }
    }

    public void keepPlayingRingtone() {
        if (defaultRingtone != null && !defaultRingtone.isPlaying()) {
            defaultRingtone.play();
        }
    }

    @Override
    public void setAudioOutput(int output) {
        mediaPlayer.pause();
        if (output == OUTPUT_PHONE_SPEAKER) {
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_VOICE_CALL);
        } else {
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        }
        try {
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            Gdx.app.error("error", "error", e);
        }
    }

    @Override
    public void stopPhoneCallAudio() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
        }
    }

    @Override
    public void playHangupSound() {
        ToneGenerator toneGenerator = new ToneGenerator(AudioManager.STREAM_VOICE_CALL, 80); //80 is volume
        toneGenerator.startTone(ToneGenerator.TONE_PROP_PROMPT, 200); //200 is length
    }


    @Override
    public void startPhoneCallAudio() {
        mediaPlayer = new MediaPlayer();
        try {
            AndroidFileHandle aHandle = (AndroidFileHandle) Gdx.files.internal("audio/governmentCall.mp3");
            AssetFileDescriptor descriptor = aHandle.getAssetFileDescriptor();
            mediaPlayer.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_VOICE_CALL);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            Gdx.app.error("error", "error", e);
        }
    }

    @Override
    public void vibrateIfShouldOnRing() {
        AudioManager audioManager = (AudioManager) activity.getApplicationContext()
                .getSystemService(Context.AUDIO_SERVICE);
        boolean bVibrateRing = audioManager
                .shouldVibrate(AudioManager.VIBRATE_TYPE_RINGER);
        int vibrate = Settings.System.getInt(activity.getApplicationContext().getContentResolver(),
                "vibrate_when_ringing", 0);
        if (vibrate > 0 || bVibrateRing) {
            Gdx.input.vibrate(1000);
        }
    }

}
